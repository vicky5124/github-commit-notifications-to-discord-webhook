# How to run
- Download the executable from [releases](https://gitlab.com/nitsuga5124/github-commit-notifications-to-discord-webhook/-/releases)
+ Alternative, run `cargo build --release` and use the executable or binary from `target/release/`

- Rename `config_example.toml` to `config.toml`
- Fill the configuration with the required information. (Do not touch the [lock] section)
- Run the executable.
