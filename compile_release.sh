#!/bin/sh
cargo build --release
cross build --release --target x86_64-pc-windows-gnu
cross build --release --target x86_64-unknown-linux-gnu --features vendored-openssl
cross build --release --target armv7-unknown-linux-gnueabihf --features vendored-openssl
# i don't care about macOS users
