use std::collections::HashMap;
use std::time::Duration;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::io::SeekFrom;

use toml::Value;
use log::*;
use pretty_env_logger::formatted_builder;
use serde::{
    Serialize,
    Deserialize,
};

pub type GitHubResponse = Vec<GitHubResponseElement>;

#[derive(Serialize, Deserialize, Debug)]
pub struct GitHubResponseElement {
    sha: String,
    commit: Commit,
    author: Option<Committer>,
    html_url: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Commit {
    author: Author,
    message: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Author {
    date: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Committer {
    login: String,
    avatar_url: String,
    html_url: String,
}

#[derive(Debug, Deserialize, Serialize, Default)]
pub struct Embed {
    pub title: String,
    pub url: String,
    pub description: String,
    pub author: EmbedAuthor,
    pub timestamp: String,

    #[serde(default)]
    pub footer: EmbedFooter,
}

#[derive(Debug, Deserialize, Serialize, Default)]
pub struct EmbedAuthor {
    pub name: String,
    pub icon_url: String,
    pub url: String,
}

#[derive(Debug, Deserialize, Serialize, Default)]
pub struct EmbedFooter {
    pub text: String,
}


fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut file = OpenOptions::new()
        .read(true)
        .write(true)
        .append(false)
        .create(true)
        .open("config.toml")?;

    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let mut configuration = contents.parse::<Value>().unwrap();

    let base_level = configuration["data"]["log_level"].as_str().unwrap();

    let mut builder = formatted_builder();
    let level = match base_level {
        "error" => LevelFilter::Error,
        "warn" => LevelFilter::Warn,
        "info" => LevelFilter::Info,
        "debug" => LevelFilter::Debug,
        "trace" => LevelFilter::Trace,
        _ => LevelFilter::Trace,
    };

    builder.filter_module("gh_repository_updates", level);

    builder.init();

    info!("Event logger initialized with the level {}", level);

    if let Err(why) = checker_loop(&mut file, &mut configuration) {
        error!("An error happened during execution");
        error!("{}", why);
    }

    Ok(())
}

fn checker_loop(file: &mut File, configuration: &mut Value) -> Result<(), Box<dyn std::error::Error>> {
    let webhook_url = if let Some(x) = configuration["data"]["webhook_url"].as_str() { x.to_string() } else {
        error!("Specify a valid discord webhook URL on the webhook_url field in `config.toml`");
        println!("Specify a valid discord webhook URL on the webhook_url field in `config.toml`");
        return Ok(());
    };

    info!("Webhook URL: `{}`", webhook_url);

    let repository = if let Some(x) = configuration["data"]["repository"].as_str() { x.to_string() } else {
        error!("Specify a repository on the repository field in `config.toml`\nExample: \"discord/discord-api-docs\"");
        println!("Specify a repository on the repository field in `config.toml`\nExample: \"discord/discord-api-docs\"");
        return Ok(());
    };

    let api_url = format!("https://api.github.com/repos/{}/commits", &repository);

    info!("Repository API URL: `{}`", api_url);

    let known_repos_raw = if let Some(x) = configuration["lock"]["known_repos"].as_array() { x } else {
        error!("Do not touch the lock section of `config.toml`");
        error!("Please, reset it to the default one.");
        panic!("Do not touch the lock section of `config.toml`");
    };

    let client = reqwest::blocking::Client::builder();

    let known_repos = known_repos_raw.iter().map(|i| i.as_str().unwrap_or_default().to_string()).collect::<Vec<_>>();

    if !known_repos.contains(&repository) {
        info!("{} is a previously unknown repository, skipping first iteration.", &repository);

        let resp = client
            .user_agent("gh_repository_updates_to_discord_webhook")
            .build()?
            .get(&api_url)
            .send()?
            .json::<GitHubResponse>()?;

        {
            let sent_sha_raw = if let Some(x) = configuration["lock"]["sent_sha"].as_array_mut() { x } else {
                error!("Do not touch the lock section of `config.toml`");
                error!("Please, reset it to the default one.");
                panic!("Do not touch the lock section of `config.toml`");
            };

            for i in resp {
                sent_sha_raw.push(Value::from(i.sha.to_string()));
                trace!("Written sha `{}` to already send sha's", &i.sha);
            }
        }

        {
            let known_repos_raw = if let Some(x) = configuration["lock"]["known_repos"].as_array_mut() { x } else {
                error!("Do not touch the lock section of `config.toml`");
                error!("Please, reset it to the default one.");
                panic!("Do not touch the lock section of `config.toml`");
            };
            known_repos_raw.push(Value::from(repository.to_string()));
            trace!("Written repo `{}` to already known repositories", &repository);
        }

        debug!("Successfully skipped `{}`", &repository);
    }

    let time = 120;

    loop {
        let sent_sha_raw = if let Some(x) = configuration["lock"]["sent_sha"].as_array_mut() { x } else {
            error!("Do not touch the lock section of `config.toml`");
            error!("Please, reset it to the default one.");
            panic!("Do not touch the lock section of `config.toml`");
        };

        let sent_sha = sent_sha_raw.iter().map(|i| i.as_str().unwrap_or_default().to_string()).collect::<Vec<_>>();

        let client = reqwest::blocking::Client::builder();

        trace!("Requesting to GitHub API with URL `{}`", &api_url);
        let resp = client
            .user_agent("gh_repository_updates_to_discord_webhook")
            .build()?
            .get(&api_url)
            .send()?
            .json::<GitHubResponse>()?;
        trace!("Succecssful request to GitHub API with URL `{}`", &api_url);

        let embeds = resp.iter().filter_map(|resp| {
            let mut embed = Embed::default();

            if let Some(a) = &resp.author {
                let author = EmbedAuthor {
                    name: a.login.to_string(),
                    icon_url: a.avatar_url.to_string(),
                    url: a.html_url.to_string(),
                };
                embed.author = author;
            }

            embed.description = resp.commit.message.to_string();
            embed.timestamp = resp.commit.author.date.to_string();
            embed.footer.text = resp.sha.to_string();

            embed.title = "New Commit".to_string();
            embed.url = resp.html_url.to_string();
            
            if !sent_sha.contains(&resp.sha) {
                info!("Preparing to send sha {}", &resp.sha);

                sent_sha_raw.push(Value::from(resp.sha.to_string()));
                trace!("Written sha `{}` to already send sha's", &resp.sha);

                Some(embed)
            } else {
                None
            }
        }).collect::<Vec<_>>();

        drop(sent_sha_raw);

        if !embeds.is_empty() {
            let mut map = HashMap::new();
            map.insert("embeds", embeds);

            trace!("Sending to webhook the following data:\n{:#?}", &map);

            reqwest::blocking::Client::new()
                .post(&webhook_url)
                .json(&map)
                .send()?;

            info!("Successfully sent webhook.");
        }

        let config = configuration.to_string();
        trace!("Writing to config:\n{}", &config);
        file.seek(SeekFrom::Start(0))?;
        file.set_len(0)?;
        write!(file, "{}", config)?;
        trace!("Successfully written to the config file.");

        debug!("Iteration happened, waiting {} seconds for the next one.", time);
        std::thread::sleep(Duration::from_secs(time));
        trace!("{} seconds finished; Running iteration.", time)
    }
}
